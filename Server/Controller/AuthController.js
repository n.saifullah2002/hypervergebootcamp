import { UserModel } from "../Models/UserModel.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

function generateAccessToken(email) {
  return jwt.sign({ _id: email }, process.env.ACCESS_SECRET, {
    expiresIn: "10m",
  });
}

function generateRefreshToken(email) {
  return jwt.sign({ _id: email }, process.env.REFRESH_SECRET, {
    expiresIn: "1d",
  });
}

export async function registerUser(req, res) {
  const {
    firstName,
    lastName,
    email,
    password,
    phoneNumber,
    address,
    operator,
  } = req.body;
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt);
  try {
    let accessToken = generateAccessToken(email);
    let refreshToken = generateRefreshToken(email);
    await UserModel.create({
      firstName,
      lastName,
      email,
      password: hashPassword,
      phoneNumber,
      address,
      operator,
      token: refreshToken,
    });
    res.cookie("rftoken", {
      httpOnly: true,
      sameSite: "None",
      secure: true,
      maxAge: 24 * 60 * 60 * 1000,
      value: refreshToken,
    });
    return res.json({ accessToken });
  } catch (error) {
    error.code === 11000
      ? res.status(400).send("invalid creds")
      : res.status(500);
  }
}

export async function loginUser(req, res) {
  const { email, password } = req.body;
  try {
    let userData = await UserModel.findOne({ email: email });
    if (!userData) return res.status(500).json("invalid credrentials");
    const match = await bcrypt.compare(password, userData.password);

    if (!match) return res.status(401).json({ msg: "Wrong Password" });

    let accessToken = generateAccessToken(email);
    let refreshToken = generateRefreshToken(email);
    await UserModel.findOneAndUpdate({ email: email }, { token: refreshToken });

    res.cookie("rftoken", {
      httpOnly: true,
      sameSite: "None",
      secure: true,
      maxAge: 24 * 60 * 60 * 1000,
      value: refreshToken,
    });
    return res.json({ accessToken });
  } catch (error) {
    res.status(500).send("invalid ");
  }
}

export async function refreshToken(req, res) {
  if (!req.cookies["rftoken"])
    return res.status(401).json({ message: "Unauthorized" });

  const refreshToken = req.cookies.rftoken.value;
  let user = await UserModel.find({ token: refreshToken });
  if (!user) return res.status(401).json({ message: "Unauthorized" });
  jwt.verify(refreshToken, process.env.REFRESH_SECRET, (err, decoded) => {
    if (err) {
      return res.status(401).json({ message: "Unauthorized" });
    } else {
      const accessToken = generateAccessToken(decoded.email);
      return res.json({ accessToken });
    }
  });
}

export async function logout(req, res) {
  if (!req.cookies.rftoken)
    return res.status(401).json({ message: "Unauthorized" });
  const refreshToken = req.cookies.rftoken;
  jwt.verify(refreshToken, process.env.REFRESH_SECRET, async (err, decoded) => {
    await UserModel.findOneAndUpdate({ email: decoded.email }, { token: "" });
  });
  res.sendStatus(204);
}

export async function updateDetail(req, res) {
  await UserModel.findOneAndUpdate({ email: req.user.email }, req.body);
}
