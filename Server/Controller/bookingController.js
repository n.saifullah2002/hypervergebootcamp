// /book
// /cancel
// /view_bookings
import { bookings } from "../Models/BookingModel.js";
import { UserModel } from "../Models/UserModel.js";
import SSE from "express-sse";

const sse = new SSE(["dummy data"]);
export function initSSE(req, res) {
  sse.init(req, res);
}

export async function viewbookings(req, res) {
  let { busId, date } = req.body;
  let operator = await UserModel.find({ email: req.user.email });
  let booking = await bookings
    .find({ busId: busId, operatorName: operator.operator.travel.travelsName })
    .lean();
  let bookingDetails = [];
  for (const x of booking) {
    if (x.departureDetails.date == date) bookingDetails.push(x);
  }
  bookingDetails.length > 0
    ? res.status(200).json(bookingDetails)
    : res.status(200).json("no bookings found");
}

export async function mybookings(req, res) {
  let userData = await UserModel.find({ email: req.user.email });
  let booking = await bookings.find({ customerId: userData.id });
  booking.length > 0
    ? res.status(200).json(booking)
    : res.status(400).json("no bookings found");
}

export async function cancelBooking(req, res) {
  let userData = await UserModel.find({ email: req.user.email });
  let booking = bookings.findOneandUpdate(
    { customerId: userData.id, id: req.params.id },
    { status: "cancelled" }
  );
  booking.length > 0
    ? res.status(200).json(booking)
    : res.status(200).json("no bookings found");
}

export async function viewTicket(req, res) {
  let userData = await UserModel.find({ email: req.user.email });
  let booking = await bookings.find({
    customerId: userData.id,
    id: req.params.id,
  });
}

export async function bookTicket(req, res) {
  let userData = await UserModel.find({ email: req.user.email });
  let {
    busId,
    passengerDetails,
    email,
    phoneNumber,
    fare,
    status,
    bookingDate,
    seats,
    departureDetails,
    arrivalDetails,
    pnr,
    isInsurance,
  } = req.body;
  try {
    let booking = await bookings.create({
      busId,
      passengerDetails,
      email,
      phoneNumber,
      fare,
      status,
      bookingDate,
      seats,
      departureDetails,
      arrivalDetails,
      pnr,
      isInsurance,
      customerId: useerData["_id"],
    });
    res.status(200).json(booking);
  } catch (e) {
    res.status(500).json(e);
  }
}

export async function seatsOnhold(request, respsonse, next) {
  const { seats, busId } = request.body;
  sse.send(seats, busId);
}
