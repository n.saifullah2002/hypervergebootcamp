// busRouter.get("/list");
// busRouter.post("/add_bus");
// busRouter.post("/newroute");
// busRouter.get("/routes");
import { busModel } from "../Models/BusModel.js";
import { routeModel } from "../Models/RouteModel.js";
export async function findBuses(req, res) {
  let { source, destination } = req.body;
  let routeCodes = await routeModel
    .find(
      {
        $or: [
          { 'source.city': { $eq: source } },
          { stops: { $elemMatch: { name: { $eq: source } } } },
        ],
        $or: [
          { 'destination.city': { $eq: destination } },
          { stops: { $elemMatch: { name: { $eq: destination } } } },
        ],
      },
      { routeCode: 1}
    )
    .lean();
  console.log(routeCodes)
  let availableBuses = await busModel.find({
    routeId: { $in: routeCodes.map((object) => object["_id"]) },
  });
  res.status(200).json(availableBuses);
}

export async function addBus(req, res) {
  let {
    operatorName,
    busType,
    arrivalTime,
    totalSeats,
    routeCode,
    liveTracking,
    reschedulable,
    price,
  } = req.body;
  try {
    let route = await routeModel.findOne({ routeCode: routeCode });
    await busModel.create({
      operatorName,
      busType,
      arrivalTime,
      totalSeats,
      liveTracking,
      reschedulable,
      routeId: route["_id"],
      price,
    });
    res.status(200).json("sucess");
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

export async function addRoute(req, res) {
  let { routeCode, duration, source, destination, stops } = req.body;
  console.log({ routeCode, duration, source, destination, stops });
  try {
    await routeModel.create({
      routeCode,
      duration,
      source,
      destination,
      stops: stops,
    });
    res.status(200).json("sucess");
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
}

export async function listRoutes(req, res) {
  let availableRoutes = await routeModel.find().lean();
  res.status(200).json(availableRoutes);
}
