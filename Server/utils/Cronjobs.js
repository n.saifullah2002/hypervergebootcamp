import cron from "node-cron";
import { bookings } from "../Models/PreviousBookingModel";

export const BookingsManager = cron.schedule("0 59 23 * *", () => {
  // let Time = new Date().toLocaleTimeString()
  let currentDate = new Date().toLocaleDateString();
  bookings.update(
    {
      $and: [
        ("arrivalDetails.date": currentDate),
        (status: { $ne: "cancelled" }),
      ],
    },
    { status: "completed" }
  );
});


/*
 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *
 */