import express from "express";
import dbConnect from "./utils/dbConnection.js";
import dotenv from "dotenv";
import { UserModel } from "./Models/UserModel.js";
import router from "./Routes/AuthRoute.js";
import busRouter from "./Routes/BusRoutes.js";
import cookies from "cookie-parser";
import bookingRouter from "./Routes/BookingRoutes.js";
import cors from "cors";
import helmet from "helmet";
import {check , validationResult} from 'express-validator'
import errorHandler from "./utils/errorHandler.js";
dotenv.config();

const app = express();

//used helmet to defend against basic xss
app.use(helmet());
app.use(cookies());
app.use(express.json());
//allows cross orgin from reactClient
app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
    allowedHeaders: ["Authorization"],
  })
);

await dbConnect();

app.use(function (err, req, res, next) {
    return res.status(500).json({
        error: errorHandler(err) || "Something went wrong!",
    });
});

app.get("/", async (req, res) => {
  return res.json("Hello World!");
});

app.use("/bus", busRouter);
app.use("/auth", router);
app.use("/api/", bookingRouter);

app.listen(8080, () => {
  console.log("listeng to port ");
});
