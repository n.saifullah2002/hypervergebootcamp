import { Router } from "express";
import {
  registerUser,
  loginUser,
  refreshToken,
  logout,
  updateDetail,
} from "../Controller/AuthController.js";
import { authenticateToken } from "../Middleware/AuthMiddleware.js";

// Initialization
const router = Router();

// Requests
router.post("/register", registerUser);
router.post("/login", loginUser);
router.post("/refresh", refreshToken);
router.delete("/logout", logout);
router.use(authenticateToken)
router.post("/update", updateDetail);
export default router;
