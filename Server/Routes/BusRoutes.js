import { Router } from "express";
import { authenticateToken } from "../Middleware/AuthMiddleware.js";
import {
  addBus,
  findBuses,
  addRoute,
  listRoutes,
} from "../Controller/BusController.js";
let busRouter = new Router();
busRouter.use(authenticateToken);
busRouter.post("/buses", findBuses);
busRouter.post("/add_bus", addBus);
busRouter.post("/newroute", addRoute);
busRouter.get("/routes", listRoutes);

export default busRouter;
