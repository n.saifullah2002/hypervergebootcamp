import { Router } from "express";
import {
  bookTicket,
  viewbookings,
  cancelBooking,
  viewTicket,
  initSSE,
  seatsOnhold,
} from "../Controller/bookingController.js";
import { authenticateToken } from "../Middleware/AuthMiddleware.js";

let bookingRouter = new Router();

bookingRouter.use(authenticateToken);
bookingRouter.post("/book", bookTicket);
bookingRouter.post("/cancel", cancelBooking);
bookingRouter.post("/view_bookings", viewbookings);
bookingRouter.post("/view_ticket:id", viewTicket);
bookingRouter.get("/seats_on_hold",initSSE)
bookingRouter.post("/seats_on_hold", seatsOnhold);

export default bookingRouter;
