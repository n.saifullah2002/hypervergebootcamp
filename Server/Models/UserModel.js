import mongoose from "mongoose";
const User = new mongoose.Schema({
  firstName: {
    type: String,
    default: null,
  },
  lastName: {
    type: String,
    default: null,
  },
  email: {
    type: String,
    unique: [true, "email aldready exists"],
    required: [true, "please specify the email "],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Please fill a valid email address",
    ],
  },
  password: {
    type: String,
  },
  phoneNumber: {
    type: Number,
    maxlength: [10, "not a valid phno."],
  },
  address: {
    type: String,
  },
  token: {
    type: String,
    default: null,
  },

  operator: {
    travelsName: {
      type: String,
    },
    locations: {
      type: [String],
    },
  },
});

export var UserModel =
  mongoose.models.AuthTest || mongoose.model("AuthTest", User);
