import mongoose from "mongoose";

const Schema = mongoose.Schema;

const bookingModel = new Schema(
  {
    customerId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    busId: {
      type: Schema.Types.ObjectId,
      ref: "bus",
      required: true,
    },
    passengerDetails: [
      {
        name: { type: String, required: true },
        gender: { type: String, required: true },
        age: { type: Number, required: true },
      },
    ],
    email: {
      type: String,
      required: true,
    },
    phoneNumber: {
      type: Number,
      required: true,
    },
    fare: {
      type: Number,
      required: true,
    },
    status: {
      type: String,
      required: true,
      enum: ["Yet to Come", "Cancelled", "Completed"],
    },
    seats: {
      type: [Number],
      required: true,
    },
    departureDetails: {
      city: { type: String, required: true },
      latitude: { type: Number, required: true },
      longitude: { type: Number, required: true },
      time: { type: String, required: true },
      date: { type: Date, required: true },
    },
    arrivalDetails: {
      city: { type: String, required: true },
      latitude: { type: Number, required: true },
      longitude: { type: Number, required: true },
      time: { type: String, required: true },
      date: { type: Date, required: true },
    },
    isInsurance: {
      type: Boolean,
      required: false,
    },
    pnr: {
      type: String,
      required: true,
      unique: true,
    },
  },
  { timestamps: true }
);

export const bookings =
  mongoose.models.bookings || mongoose.model("bookings", bookingModel);
