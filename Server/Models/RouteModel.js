import mongoose from "mongoose";

const Routes = new mongoose.Schema({
  routeCode: {
    type: String,
    unique: true,
  },
  source: {
    city: {
      type: String,
      unique: true,
    },
    latitude: Number,
    longitude: Number,
    time: String,
  },
  destination: {
    city: {
      type: String,
      unique: true,
    },
    latitude: Number,
    longitude: Number,
    time: String,
  },
  stops: [
    {
      city: {
        type: String,
        unique: true,
      },
      latitude: Number,
      longitude: Number,
      time: String,
    },
  ],
  duration: {
    type: Number,
    required: true,
  },
});

export const routeModel =
  mongoose.models.routes || mongoose.model("routes", Routes);
